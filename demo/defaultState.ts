import State from 'demo/state';

const defaultState: State.All = {
  debug: false,
  drawerSettings: {
    docked: true,
    drag: false,
    hover: true,
    index: 0,
    open: true
  },
  drawers: [{
    id: 'tools',
    params: {
      title: 'Tools'
    }
  },{
    id: 'settings',
    params: {
      title: 'Settings'
    }
  }],
  miscellaneous: {
    shortName: 'redig',
    title: 'UIRouter React Digest'
  },
  orientation: 'left',
  tabSettings: {
    index: 0
  },
  tabs: [{
    id: '404',
    params: {
      hidden: true,
      title: '404',
      type: '404'
    },
    sticky: true,
    tenured: true,
  }, {
    deepStateRedirect: true,
    id: 'home',
    params: {
      title: 'Home',
      type: 'home'
    },
    sticky: true,
    tenured: true,
  }, {
    deepStateRedirect: true,
    id: 'tabb',
    params: {
      swipe: true,
      title: 'Tab B',
      type: 'tabb'
    },
    sticky: true,
    tenured: true
  }]
};

export default defaultState;
