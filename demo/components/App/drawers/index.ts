import {
  ClassicComponentClass,
  ComponentClass,
  StatelessComponent
} from 'react';
import Settings from './Settings';

interface IDrawerTypes {
  [keys: string]: {
    component?: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
    contextual?: boolean;
    id?: string;
  };
}

const drawerTypes: IDrawerTypes = {
  settings: {
    component: Settings,
  },
  tools: {
    contextual: true
  }
};

export default drawerTypes;
