import {
  Orientation,
  Tab as ITab,
  Tabs as ITabs,
} from 'index';
import React, {PureComponent, SyntheticEvent} from 'react';
import './Settings.css';

export interface ISettings {
  drawerDocked: boolean;
  drawerDrag: boolean;
  drawerHover: boolean;
  onDrawerDockedToggle: (toggle?: boolean) => any;
  onDrawerDragToggle: (toggle?: boolean) => any;
  onDrawerHoverToggle: (toggle?: boolean) => any;
  onOrientationToggle: (toggle?: Orientation) => any;
  onTabAdd: (tab: ITab) => any;
  onTabRemove: (index: number) => any;
  orientation: Orientation;
  tabs: ITabs;
}

class Settings extends PureComponent<ISettings> {
  public render() {
    return (
      <div styleName='settings'>
        <h4>Tabs</h4>
        <form>
          <button
            id='ui-add-tabs'
            name='Add Tab'
            onClick={this._whenTabAdd}
          >
            Add Tab
          </button>
          <br />
          <br />
          <button
            id='ui-remove-tabs'
            name='Remove Tab'
            onClick={this._whenTabRemove}
          >
            Remove Tab
          </button>
        </form>
        <h4>Preferences</h4>
        <form>
          <label htmlFor='ui-docked'>
            <input
              checked={this.props.drawerDocked}
              id='ui-docked'
              max='1'
              min='0'
              name='docked'
              onChange={this._whenDrawerDockedToggle}
              step='1'
              type='checkbox'
            />
            Drawer Docked
          </label>
          <br />
          <label htmlFor='ui-drag'>
            <input
              checked={this.props.drawerDrag}
              id='ui-drag'
              max='1'
              min='0'
              name='drag'
              onChange={this._whenDrawerDragToggle}
              step='1'
              type='checkbox'
            />
            Drawer Drag
          </label>
          <br />
          <label htmlFor='ui-hover'>
            <input
              checked={this.props.drawerHover}
              id='ui-hover'
              max='1'
              min='0'
              name='hover'
              onChange={this._whenDrawerHoverToggle}
              step='1'
              type='checkbox'
            />
            Drawer Hover
          </label>
          <br />
          <label htmlFor='ui-orientation'>
            <input
              id='ui-orientation'
              max='1'
              min='0'
              name='orientation'
              onChange={this._whenOrientationToggle}
              step='1'
              type='range'
              value={'left' === this.props.orientation ? 0 : 1}
            />
            Orientation
          </label>
        </form>
      </div>
    );
  }

  private _whenDrawerDockedToggle = () => {
    this.props.onDrawerDockedToggle();
  }

  private _whenDrawerDragToggle = () => {
    this.props.onDrawerDragToggle();
  }

  private _whenDrawerHoverToggle = () => {
    this.props.onDrawerHoverToggle();
  }

  private _whenOrientationToggle = () => {
    this.props.onOrientationToggle();
  }

  private _whenTabAdd = (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault();
    this.props.onTabAdd({id: undefined});
  }

  private _whenTabRemove = (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault();
    this.props.onTabRemove(this.props.tabs.length - 1);
  }
}

export default Settings;
