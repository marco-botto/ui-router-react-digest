import React from 'react';
import renderer from 'react-test-renderer';
import defaults from 'src/defaults';
import Settings from '../Settings';

const drawerDocked = true;
const drawerDrag = false;
const drawerHover = true;
const onDrawerDockedToggle = () => {return;};
const onDrawerDragToggle = () => {return;};
const onDrawerHoverToggle = () => {return;};
const onOrientationToggle = () => {return;};
const onTabAdd = () => {return;};
const onTabRemove = () => {return;};
const orientation = 'left';

test('Settings drawer', () => {
  const component = renderer.create(
    <Settings
      drawerDocked={drawerDocked}
      drawerDrag={drawerDrag}
      drawerHover={drawerHover}
      onDrawerDockedToggle={onDrawerDockedToggle}
      onDrawerDragToggle={onDrawerDragToggle}
      onDrawerHoverToggle={onDrawerHoverToggle}
      onOrientationToggle={onOrientationToggle}
      onTabAdd={onTabAdd}
      onTabRemove={onTabRemove}
      orientation={orientation}
      tabs={defaults.tabs}
    />
  );
  const settings = component.toJSON();
  expect(settings).toMatchSnapshot();
});
