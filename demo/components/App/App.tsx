import {DSRPlugin} from '@uirouter/dsr';
import {
  hashLocationPlugin,
  // pushStateLocationPlugin,
  servicesPlugin,
  UIRouterReact
} from '@uirouter/react';
import {StickyStatesPlugin} from '@uirouter/sticky-states';
// import {Visualizer} from '@uirouter/visualizer';
import {Drawers, Orientation, Tabs} from 'index';
import React, {PureComponent} from 'react';
import UIRouterReactDigest from 'src/';
import '../../../lib/styles/main.min.css';
import drawers from './drawers';
import tabs from './tabs';

interface IAppProps {
  debug: boolean;
  drawerDocked: boolean;
  drawerDrag: boolean;
  drawerHover: boolean;
  drawerIndex: number;
  drawerOpen: boolean;
  drawers: Drawers;
  orientation: Orientation;
  onDrawerOpenToggle: (toggle: boolean) => any;
  onDrawerSelect: (index: number) => any;
  onTabSelect: (index: number) => any;
  tabIndex: number;
  tabs: Tabs;
  title: string;
}

export default class App extends PureComponent<IAppProps> {
  private _rootState = {
    name: 'root',
    params: {
      lang: {
        dynamic: true,
        value: 'en'
      }
    },
    url: '/:lang'
  };
  private _router: UIRouterReact;

  constructor (props: IAppProps) {
    super(props);
    this._router = new UIRouterReact();
    this._router.plugin(servicesPlugin);
    this._router.plugin(hashLocationPlugin);
    // this._router.plugin(pushStateLocationPlugin);
    this._router.plugin(DSRPlugin);
    this._router.plugin(StickyStatesPlugin);
    // this._router.plugin(Visualizer);

    this._router.urlRouter.when('', () => {
      this._router.stateService.go(
        this._rootState.name + '.tabs.home'
      );
    });
  }

  public render () {
    const tabOrder: Tabs = this.props.tabs.map((tab) => {
      return tabs[tab.params.type] ? {
        ...tabs[tab.params.type],
        ...tab
      } : {
        ...tabs.dynamic,
        ...tab
      };
    });

    const drawerOrder: Drawers = this.props.drawers.map((drawer) => {
      return drawers[drawer.id] ? {
        ...drawers[drawer.id],
        ...drawer
      } : drawer;
    });

    return (
      <UIRouterReactDigest
        debug={this.props.debug}
        drawerDocked={this.props.drawerDocked}
        drawerDrag={this.props.drawerDrag}
        drawerHover={this.props.drawerHover}
        drawerIndex={this.props.drawerIndex}
        drawerOpen={this.props.drawerOpen}
        drawers={drawerOrder}
        onDrawerOpenToggle={this.props.onDrawerOpenToggle}
        onDrawerSelect={this.props.onDrawerSelect}
        onTabSelect={this.props.onTabSelect}
        orientation={this.props.orientation}
        rootState={this._rootState}
        router={this._router}
        tabIndex={this.props.tabIndex}
        tabs={tabOrder}
        title={this.props.title}
      />
    );
  }
}
