import React from 'react';
import renderer from 'react-test-renderer';
import defaults from 'src/defaults';
import App from '../App';

test('Settings drawer', () => {
  const component = renderer.create(
    <App
      debug={defaults.debug}
      drawerDocked={defaults.drawerDocked}
      drawerDrag={defaults.drawerDrag}
      drawerHover={defaults.drawerHover}
      drawerIndex={defaults.drawerIndex}
      drawerOpen={defaults.drawerOpen}
      drawers={defaults.drawers}
      onDrawerOpenToggle={defaults.onDrawerOpenToggle}
      onDrawerSelect={defaults.onDrawerSelect}
      onTabSelect={defaults.onTabSelect}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={defaults.tabs}
      title={defaults.title}
    />
  );
  const app = component.toJSON();
  expect(app).toMatchSnapshot();
});
