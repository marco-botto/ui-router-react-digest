import React from 'react';
import renderer from 'react-test-renderer';
import Dynamic from '../Dynamic';

test('TabB content', () => {
  const component = renderer.create(
    <Dynamic />
  );
  const dynamic = component.toJSON();
  expect(dynamic).toMatchSnapshot();
});
