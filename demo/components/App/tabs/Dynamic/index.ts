import defaultState from 'demo/defaultState';
import {connect} from 'react-redux';
import Dynamic from './Dynamic';

const mapStateToProps = (state = defaultState) => ({});

export default connect(
  mapStateToProps
)(Dynamic);
