import {
  ClassicComponentClass,
  ComponentClass,
  StatelessComponent
} from 'react';
import E404 from './404';
import E404Tools from './404/Tools';
import Dynamic from './Dynamic';
import DynamicTools from './Dynamic/Tools';
import Home from './Home';
import HomeTools from './Home/Tools';
import TabB from './TabB';
import TabBTools from './TabB/Tools';

interface ITabTypes {
  [keys: string]: {
    component: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
    drawers: {
      [keys: string]: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
    };
  };
}

const tabTypes: ITabTypes = {
  404: {
    component: E404,
    drawers: {
      tools: E404Tools
    }
  },
  dynamic: {
    component: Dynamic,
    drawers: {
      tools: DynamicTools
    }
  },
  home: {
    component: Home,
    drawers: {
      tools: HomeTools
    }
  },
  tabb: {
    component: TabB,
    drawers: {
      tools: TabBTools
    }
  },
};

export default tabTypes;
