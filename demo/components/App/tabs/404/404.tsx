import React, {PureComponent} from 'react';
import './404.css';
import Tools from './Tools';

class E404 extends PureComponent {
  public render() {
    return (
      <div styleName='E404'>
        <h4>404</h4>
      </div>
    );
  }
}

export default E404;
