import React from 'react';
import renderer from 'react-test-renderer';
import Home from '../Home';

test('Home content', () => {
  const component = renderer.create(
    <Home />
  );
  const home = component.toJSON();
  expect(home).toMatchSnapshot();
});
