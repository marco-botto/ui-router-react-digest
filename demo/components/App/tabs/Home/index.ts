import defaultState from 'demo/defaultState';
import {connect} from 'react-redux';
import Home from './Home';

const mapStateToProps = (state = defaultState) => ({});

export default connect(
  mapStateToProps
)(Home);
