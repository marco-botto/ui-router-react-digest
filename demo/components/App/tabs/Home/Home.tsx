import React, {PureComponent} from 'react';
import './Home.css';
import Tools from './Tools';

export default class Home extends PureComponent {
  public render() {
    return (
      <div styleName='home'>
        <h4>Home</h4>
      </div>
    );
  }
}
