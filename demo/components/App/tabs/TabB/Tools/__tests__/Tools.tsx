import React from 'react';
import renderer from 'react-test-renderer';
import Tools from '../Tools';

test('TabB tools', () => {
  const component = renderer.create(
    <Tools />
  );
  const tools = component.toJSON();
  expect(tools).toMatchSnapshot();
});
