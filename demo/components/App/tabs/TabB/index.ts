import defaultState from 'demo/defaultState';
import {connect} from 'react-redux';
import TabB from './TabB';

const mapStateToProps = (state = defaultState) => ({});

export default connect(
  mapStateToProps
)(TabB);
