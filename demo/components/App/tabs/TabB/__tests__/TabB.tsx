import React from 'react';
import renderer from 'react-test-renderer';
import TabB from '../TabB';

test('TabB content', () => {
  const component = renderer.create(
    <TabB />
  );
  const tabB = component.toJSON();
  expect(tabB).toMatchSnapshot();
});
