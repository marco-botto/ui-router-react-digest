import {
  Drawers,
  Orientation,
  Tabs
} from '../index';

/* tslint:disable:no-namespace no-internal-module interface-name */
declare module State {
  export type debug = boolean;

  export type drawers = Drawers;

  export namespace drawerSettings {
    export type docked = boolean;
    export type drag = boolean;
    export type hover = boolean;
    export type index = number;
    export type open = boolean;
  }

  export namespace miscellaneous {
    export type shortName = string;
    export type title = string;
  }

  export type tabs = Tabs;

  export namespace tabSettings {
    export type index = number;
  }

  export interface All {
    debug: debug;
    drawerSettings: {
      docked: drawerSettings.docked;
      drag: drawerSettings.drag;
      hover: drawerSettings.hover;
      index: drawerSettings.index;
      open: drawerSettings.open;
    };
    drawers: Drawers;
    miscellaneous: {
      shortName: miscellaneous.shortName;
      title: miscellaneous.title;
    };
    orientation: orientation;
    tabSettings: {
      index: tabSettings.index;
    };
    tabs: Tabs;
  }
}
/* tslint:enable:no-namespace no-internal-module interface-name */

export default State;
