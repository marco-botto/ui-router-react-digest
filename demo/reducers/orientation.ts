import {IOrientationAction} from 'demo/actions/orientation';
import defaultState from 'demo/defaultState';
import {Orientation} from 'index';

const orientation = (
  state = defaultState.orientation,
  action: IOrientationAction
): Orientation => {
  switch (action.type) {
    case 'ORIENTATION':
      if (action.position === 'left') {
        return 'left';
      }
      if (action.position === 'right') {
        return 'right';
      }
      if (typeof action.position === 'undefined' || action.position === null) {
        return state === 'left' ? 'right' : 'left';
      }
      return state;
    default:
      return state;
  }
};

export default orientation;
