import {combineReducers} from 'redux';
import debug from './debug';
import drawers from './drawers';
import drawerSettings from './drawerSettings';
import miscellaneous from './miscellaneous';
import orientation from './orientation';
import tabs from './tabs';
import tabSettings from './tabSettings';

const reducers = {
  debug,
  drawerSettings,
  drawers,
  miscellaneous,
  orientation,
  tabSettings,
  tabs
};

const combinedReducers = combineReducers(reducers as any);

export default combinedReducers;
