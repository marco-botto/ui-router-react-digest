import defaultState from 'demo/defaultState';
import State from 'demo/state';

function tabs(
  state = defaultState.tabs,
  action: any
): State.tabs {
  switch (action.type) {
    default:
      return state;
  }
}

export default tabs;
