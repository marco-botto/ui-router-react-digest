import {
  ITabAddAction,
  ITabRemoveAction
} from 'demo/actions/tabs';
import defaultState from 'demo/defaultState';
import State from 'demo/state';
import shortid from 'shortid';

const tabDefaults = {
  deepStateRedirect: true,
  params: {
    title: 'New Tab',
    type: 'dynamic'
  },
  sticky: true
};

function tabs(
  state = defaultState.tabs,
  action: ITabAddAction |
    ITabRemoveAction
): State.tabs {
  switch (action.type) {
    case 'TAB_ADD':
      return [...state, {...tabDefaults, ...action.tab, id: action.tab.id || shortid.generate()}];
    case 'TAB_REMOVE':
      return typeof action.index === 'number' && state[action.index] ?
        state.filter((tab, index) => (index !== action.index)) :
        state;
    default:
      return state;
  }
}

export default tabs;
