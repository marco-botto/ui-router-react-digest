import {Orientation} from 'index';

export interface IOrientationAction {
  position: Orientation;
  type: 'ORIENTATION';
}

export const onOrientationToggle = (position: Orientation): IOrientationAction => ({
  position,
  type: 'ORIENTATION'
});
