beforeAll(() => {
  process.env = {};
});

afterEach(() => {
  process.env = {};
  jest.resetModules();
});

test('Test production', () => {
  expect(JSON.stringify(
    {...require('../index'), _reactInternalInstance: 'censored'}
  )).toMatchSnapshot();
});

test('Test development', () => {
  process.env.NODE_ENV = 'development';
  expect(JSON.stringify(
    {...require('../index'), _reactInternalInstance: 'censored'}
  )).toMatchSnapshot();
});
