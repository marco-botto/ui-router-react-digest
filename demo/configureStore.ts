import defaultState from 'demo/defaultState';
import reducers from 'demo/reducers';
import State from 'demo/state';
import {createStore} from 'redux';
import {loadState, saveState} from './localStorage';

const store = createStore(
  reducers,
  {
    ...defaultState,
    ...loadState()
  }
);

store.subscribe(() => {
  saveState(store.getState() as State.All);
});

export default store;
