import App from 'demo/components/App';
import configureStore from 'demo/configureStore';
import React, {ReactType} from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import {Provider} from 'react-redux';

const rootEl = document.getElementById('app') as HTMLElement ||
  document.createElement('div') as HTMLElement;

const render = (Component: ReactType) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={configureStore}>
        <Component />
      </Provider>
    </AppContainer>,
    rootEl,
    () => {
      // console.log(this); // tslint:disable-line
    }
  );
};

if (process.env.NODE_ENV === 'development') {
  /*System.import('react-axe').then((axe: any) => {
    axe(React, ReactDOM, 1000);
  });/**/

  /*System.import('why-did-you-update').then(({whyDidYouUpdate}: any) => {
    whyDidYouUpdate(React, {
      collapseComponentGroups: true,
      exclude: /^(UIView|DrawerContext|b)$/,
      groupByComponent: true
    });
  });/**/

  if (module.hot) {
    module.hot.accept('./components/App', () => {
      System.import('./components/App').then((module: any) => {
        render(module.default);
      });
    });
  }
}

export default render(App);
