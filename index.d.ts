
import {
  ReactStateDeclaration,
  ReactViewDeclaration
} from '@uirouter/react'
import PropTypes from 'prop-types';
import React, {
  ClassicComponentClass,
  Component,
  ComponentClass,
  PureComponent,
  SFC,
  StatelessComponent
} from 'react';

export interface RootState extends ReactStateDeclaration {
  name: string;
  [keys: string]: any;
}

export interface Drawer extends ReactStateDeclaration {
  component?: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
  contextual?: boolean;
  id: string;
  params?: {
    title?: string
  };
}
export interface Drawers extends Array<Drawer> {} // tslint:disable-line:no-empty-interface

export interface Tab extends ReactStateDeclaration {
  abstract?: boolean;
  component?: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
  deepStateRedirect?: boolean;
  drawers?: {
    [key: string]: StatelessComponent<any> | ComponentClass<any> | ClassicComponentClass<any>;
  };
  id: string;
  params?: {
    [key: string]: any;
  };
  sticky?: boolean;
  tenured?: boolean;
}
export interface Tabs extends Array<Tab> {} // tslint:disable-line:no-empty-interface
export interface TabsMap {
  [keys: string]: Tab;
}

export interface Views {
  [views: string]: ReactViewDeclaration;
}

export type Orientation = 'left' | 'right';

export interface UIRouterReactDigestProps {
    debug?: boolean;
    drawerDocked?: boolean;
    drawerDrag?: boolean;
    drawerFooter?: ClassicComponentClass | ComponentClass | SFC | StatelessComponent;
    drawerHeader?: ClassicComponentClass | ComponentClass | SFC | StatelessComponent;
    drawerHover?: boolean;
    drawerIndex?: number;
    drawerOpen?: boolean;
    drawers: IDrawers;
    footer?: ClassicComponentClass | ComponentClass | SFC | StatelessComponent;
    header?: ClassicComponentClass | ComponentClass | SFC | StatelessComponent;
    onDrawerSelect?: (index: number) => void;
    onDrawerOpenToggle?: (bool?: boolean) => void;
    onTabSelect?: (index: number) => void;
    orientation?: Orientation;
    plugins?: any;
    rootState?: IRootState;
    router?: UIRouterReact;
    tabIndex?: number;
    tabs: ITabs;
    title?: string;
}
export default class UIRouterReactDigest extends PureComponent<UIRouterReactDigestProps> {
    static propTypes: {
        debug: PropTypes.Requireable<any>;
        drawerDocked: PropTypes.Requireable<any>;
        drawerDrag: PropTypes.Requireable<any>;
        drawerFooter: PropTypes.Requireable<any>;
        drawerHeader: PropTypes.Requireable<any>;
        drawerHover: PropTypes.Requireable<any>;
        drawerIndex: PropTypes.Requireable<any>;
        drawerOpen: PropTypes.Requireable<any>;
        drawers: PropTypes.Validator<any>;
        header: PropTypes.Requireable<any>;
        onDrawerOpenToggle: PropTypes.Requireable<any>;
        onDrawerSelect: PropTypes.Requireable<any>;
        onTabSelect: PropTypes.Requireable<any>;
        orientation: PropTypes.Requireable<any>;
        rootState: PropTypes.Requireable<any>;
        router: PropTypes.Requireable<any>;
        tabIndex: PropTypes.Requireable<any>;
        tabs: PropTypes.Validator<any>;
        title: PropTypes.Requireable<any>;
    };
    static defaultProps: {
        [key: string]: any;
    };
    private _router;
    constructor(props: IIndexProps);
    componentWillReceiveProps({tabs, tabIndex}: IIndexProps): void;
    componentWillUpdate({tabs, tabIndex}: IIndexProps): void;
    render(): JSX.Element;
    private _title;
}
