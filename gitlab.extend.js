const path = require('path');
const express = require('express');
const app = require('webpack-digest/lib/express/app.js');

console.log(path.resolve(path.join(process.cwd(), 'public')));

function route (app) {
  app.use('/ui-router-react-digest/', express.static(path.join(process.cwd(), 'public')))
  .all('/ui-router-react-digest/*', function(request, response){
    response.sendFile(path.join(process.cwd(), 'public', 'index.html'));
  })

  return app;
}

const server = require('webpack-digest/lib/express/server.js');

server(route(app));
