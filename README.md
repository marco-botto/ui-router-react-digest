[![Build Status](https://gitlab.com/wallzero/ui-router-react-digest/badges/master/build.svg?maxAge=3600)](https://gitlab.com/wallzero/ui-router-react-digest/commits/master)
[![Coverage Report](https://gitlab.com/wallzero/ui-router-react-digest/badges/master/coverage.svg?maxAge=3600)](https://wallzero.gitlab.io/ui-router-react-digest/coverage)
[![NPM Version](http://img.shields.io/npm/v/ui-router-react-digest.svg?maxAge=86400)](https://www.npmjs.org/package/ui-router-react-digest)
[![License](https://img.shields.io/npm/l/ui-router-react-digest.svg?maxAge=2592000)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-digest.svg?type=shield&maxAge=3600)](https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-digest?ref=badge_shield)
[![Canonical Code Style](https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000)](https://github.com/gajus/canonical)

[![Commitizen Friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000)](https://commitizen.github.io/cz-cli/)
[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000)](https://gitlab.com/hyper-expanse/semantic-release-gitlab#readme)
