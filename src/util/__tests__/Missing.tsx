import React from 'react';
import renderer from 'react-test-renderer';
import Missing from '../Missing';

test('Default missing indicator', () => {
  const component = renderer.create(
    <Missing />
  );
  const missing = component.toJSON();
  expect(missing).toMatchSnapshot();
});
