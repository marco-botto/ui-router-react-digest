import React, {PureComponent} from 'react';

export default class Missing extends PureComponent {
  public render() {
    return (
      <span>
        <i className='fa fa-times-circle' /> Missing Template
      </span>
    );
  }
}
