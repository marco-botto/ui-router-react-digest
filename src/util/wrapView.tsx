import React, {PureComponent, ReactType} from 'react';

/*
  UIView passes uneccesary props to the immediate child which causes a rerender.
  This fixes this for now.
*/
export default function (Component: ReactType) {
  return class Test extends PureComponent {
    public render() {
      const {resolves, transition, ...props}: {[key: string]: any} = this.props;
      return <Component {...props}/>;
    }
  };
}
