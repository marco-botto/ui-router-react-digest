import {ReactStateDeclaration, ReactViewDeclaration, UIRouterReact} from '@uirouter/react';
import {
  Drawers as IDrawers,
  RootState,
  Tab as ITab,
  Views as IViews,
} from 'index';
import {ClassicComponentClass, ComponentClass, ReactType, StatelessComponent} from 'react';
import App from 'src/components/App';
import Tabs from 'src/components/Tabs';
import Missing from './Missing';
import wrapView from './wrapView';

export interface IState extends ReactStateDeclaration {
  abstract?: boolean;
  name: string;
}

export function initRoot(
  rootState: RootState,
  drawers: IDrawers,
  header: ReactType,
  drawerFooter: ReactType,
  drawerHeader: ReactType
): ReactStateDeclaration[] {
  const root: IViews = {
    tabs: {
      component: wrapView(Tabs)
    }
  };

  drawers.forEach((drawer) => {
    root[drawer.id] = {component: wrapView(drawer.component || Missing)};
  });

  if (header) {
    root.header = {component: wrapView(header)};
  }

  if (drawerHeader) {
    root.drawerHeader = {component: wrapView(drawerHeader)};
  }

  if (drawerFooter) {
    root.footer = {component: wrapView(drawerFooter)};
  }

  return [{
    ...rootState,
    abstract: true,
    component: wrapView(App)
  }, {
    abstract: true,
    name: rootState.name + '.tabs',
    views: root
  }] as IState[];
}

export function initTab(
  tab: ITab,
  shortName: string,
  drawers: IDrawers,
  footer: ReactType
): ReactStateDeclaration {
  const route = {
    [tab.id]: {
      component: wrapView(tab.component || Missing) as ReactType
    }
  };

  if (footer) {
    route.footer = {component: wrapView(footer)};
  }

  drawers.forEach((drawer) => {
    if (drawer.contextual === true) {
      const viewName = tab.id + '_' + drawer.id + '@' + shortName;

      route[viewName] = {
        component: (tab.drawers && tab.drawers[drawer.id] ?
          tab.drawers[drawer.id] : Missing) as ReactType
      };
    }
  });

  return {
    component: tab.component,
    deepStateRedirect: tab.deepStateRedirect,
    name: generateStateName(shortName, tab.id),
    params: {
      ...tab.params,
      id: {
        squash: true,
        value: tab.id
      }
    },
    sticky: tab.sticky,
    url: '/' + tab.id,
    views: route
  } as any;
}

export function generateStateName(
  shortName: string,
  tabName: string | number
) {
  return shortName + '.tabs.' + tabName;
}
