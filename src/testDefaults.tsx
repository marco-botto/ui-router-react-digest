export default {
  drawers: [{
    contextual: true,
    id: 'tools',
    params: {
      title: 'Tools'
    }
  }, {
    component: () => {
      return <div>Settings</div>;
    },
    id: 'settings',
    params: {
      title: 'Settings'
    }
  }],
  tabs: [{
    component: () => {
      return <div>TabA</div>;
    },
    deepStateRedirect: true,
    drawers: {
      tools: () => {
        return <div>TabA Tools</div>;
      },
    },
    id: 'taba',
    params: {
      title: 'Tab A'
    },
    sticky: true
  }, {
    component: () => {
      return <div>TabB</div>;
    },
    deepStateRedirect: true,
    drawers: {
      tools: () => {
        return <div>TabB Tools</div>;
      },
    },
    id: 'tabb',
    params: {
      title: 'Tab B'
    },
    sticky: true
  }]
};
