import {DSRPlugin} from '@uirouter/dsr';
import {
  pushStateLocationPlugin,
  ReactStateDeclaration,
  servicesPlugin,
  UIRouter,
  UIRouterPluginBase,
  UIRouterReact,
  UIView
} from '@uirouter/react';
import {StickyStatesPlugin} from '@uirouter/sticky-states';
// import {Visualizer} from '@uirouter/visualizer';
import {
  Drawers as IDrawers,
  Orientation,
  RootState,
  Tabs as ITabs
} from 'index';
import PropTypes from 'prop-types';
import React, {
  ComponentClass,
  PureComponent,
  ReactChild,
  ReactType,
  StatelessComponent
} from 'react';
import DocumentTitle from 'react-document-title';
import shortid from 'shortid';
import DrawerFooter from 'src/components/_defaults/DrawerFooter';
import DrawerHeader from 'src/components/_defaults/DrawerHeader';
import Footer from 'src/components/_defaults/Footer';
import Header from 'src/components/_defaults/Header';
import {IAppProps} from 'src/components/App';
import defaults from 'src/defaults';
import {generateStateName, initRoot, initTab} from 'src/util/initRoutes';

require('./styles'); // tslint:disable-line:no-var-requires

export interface IIndexProps {
  debug?: boolean;
  drawerDocked?: boolean;
  drawerDrag?: boolean;
  drawerFooter?: ReactType;
  drawerHeader?: ReactType;
  drawerHover?: boolean;
  drawerIndex?: number;
  drawerOpen?: boolean;
  drawers: IDrawers;
  footer?: ReactType;
  header?: ReactType;
  onDrawerOpenToggle?: (bool?: boolean) => void;
  onDrawerSelect?: (index: number) => void;
  onTabSelect?: (index: number) => void;
  orientation?: Orientation;
  plugins?: any; // Array<UIRouterPluginBase>,
  rootState?: RootState;
  router?: UIRouterReact;
  tabIndex?: number;
  tabs: ITabs;
  title?: string;
}

export default class UIRouterReactDigest extends PureComponent<IIndexProps> {
  public static propTypes = {
    debug: PropTypes.bool,
    drawerDocked: PropTypes.bool,
    drawerDrag: PropTypes.bool,
    drawerFooter: PropTypes.func,
    drawerHeader: PropTypes.func,
    drawerHover: PropTypes.bool,
    drawerIndex: PropTypes.number,
    drawerOpen: PropTypes.bool,
    drawers: PropTypes.array.isRequired,
    header: PropTypes.func,
    onDrawerOpenToggle: PropTypes.func,
    onDrawerSelect: PropTypes.func,
    onTabSelect: PropTypes.func,
    orientation: PropTypes.string,
    rootState: PropTypes.object,
    router: PropTypes.object,
    tabIndex: PropTypes.number,
    tabs: PropTypes.array.isRequired,
    title: PropTypes.string
  };

  public static defaultProps: {[key: string]: any} = {
    debug: defaults.debug,
    drawerDocked: defaults.drawerDocked,
    drawerDrag: defaults.drawerDrag,
    drawerFooter: DrawerFooter,
    drawerHeader: DrawerHeader,
    drawerHover: defaults.drawerHover,
    drawerIndex: defaults.drawerIndex,
    drawerOpen: defaults.drawerOpen,
    footer: Footer,
    header: Header,
    onDrawerOpenToggle: defaults.onDrawerOpenToggle,
    onDrawerSelect: defaults.onDrawerSelect,
    onTabSelect: defaults.onTabSelect,
    orientation: defaults.orientation,
    rootState: defaults.rootState,
    tabIndex: defaults.tabIndex,
    title: defaults.title
  };

  private _router: UIRouterReact;

  constructor (props: IIndexProps) {
    super(props);

    // Get router instance
    if (this.props.router) {
      this._router = this.props.router;
    } else {
      this._router = new UIRouterReact();
      this._router.plugin(servicesPlugin);
      this._router.plugin(pushStateLocationPlugin);
    }

    // Generate and register root states
    initRoot(
      this.props.rootState,
      this.props.drawers,
      this.props.header,
      this.props.drawerFooter,
      this.props.drawerHeader
    ).map(state => (
      this._router.stateRegistry.register(state)
    ));

    // Generate and register initial tab states
    this.props.tabs.map((tab) => {
      const state = initTab(
        tab,
        this.props.rootState.name,
        this.props.drawers,
        this.props.footer
      );

      this._router.stateRegistry.register(state);
    });

    // Resolve current tab on load and back button
    this._router.transitionService.onSuccess(
      {to: this.props.rootState.name + '.tabs.**'},
      (transition) => {
        const id = transition.targetState().state().name.split(/\./)[2];
        this.props.tabs.findIndex((tab, index) => {
          if (tab.id === id) {
            this.props.onTabSelect(index);
            return true;
          }
          return false;
        });
      }
    );

    // In case of no route found, go to first tab
    this._router.urlRouter.otherwise((found, route) => {
      this._router.stateService.go(
        this.props.rootState.name + '.tabs.' + this.props.tabs[0].id,
        null,
        {
          exitSticky: undefined,
          location: false
        }
      );
    });
  }

  public componentWillReceiveProps({tabs = [], tabIndex = 0}: IIndexProps) {
    if (tabs.length > this.props.tabs.length) {
      let diffIndex: number;

      const diff = tabs.find((tab, index) => {
        if (this.props.tabs[index] && tab.id === this.props.tabs[index].id) {
          return false;
        }
        diffIndex = index;
        return true;
      });

      const state = initTab(
        diff,
        this.props.rootState.name,
        this.props.drawers,
        this.props.footer
      );

      this._router.stateRegistry.register(state);
      this.props.onTabSelect(diffIndex);
    } else if (tabs.length < this.props.tabs.length) {
      let diffIndex: number;

      const diff = this.props.tabs.find((tab, index) => {
        if (tabs[index] && tab.id === tabs[index].id) {
          return false;
        }
        diffIndex = index;
        return true;
      });

      if (diffIndex === this.props.tabIndex) {
        if (tabs[diffIndex + 1]) {
          this._router.stateService.go(
            generateStateName(this.props.rootState.name, tabs[diffIndex + 1].id),
            null,
            {exitSticky: undefined, location: 'replace'}
          );
          this.props.onTabSelect(diffIndex + 1);
        } else if (tabs[diffIndex - 1]) {
          this._router.stateService.go(
            generateStateName(this.props.rootState.name, tabs[diffIndex - 1].id),
            null,
            {exitSticky: undefined, location: 'replace'}
          );
          this.props.onTabSelect(diffIndex - 1);
        }
      }

      this._router.stateRegistry.deregister(
        generateStateName(this.props.rootState.name, diff.id)
      );
    }
  }

  public componentWillUpdate({tabs = [], tabIndex = 0}: IIndexProps) {
    if (tabIndex !== this.props.tabIndex) {
      this._router.stateService.go(
        generateStateName(this.props.rootState.name, this.props.tabs[tabIndex].id)
      );
    }
  }

  public render () {
    const tabs = this.props.tabs.map(tab => ({
      ...tab,
      params: {
        swipe: true,
        ...tab.params
      }
    }));

    const app = (App: ComponentClass<IAppProps>) => (
      <App
        drawerDocked={this.props.drawerDocked}
        drawerDrag={this.props.drawerDrag}
        drawerHover={this.props.drawerHover}
        drawerIndex={this.props.drawerIndex}
        drawerOpen={this.props.drawerOpen}
        drawers={this.props.drawers}
        onDrawerSelect={this.props.onDrawerSelect}
        onDrawerOpenToggle={this.props.onDrawerOpenToggle}
        onTabSelect={this.props.onTabSelect}
        orientation={this.props.orientation}
        tabIndex={this.props.tabIndex}
        tabs={tabs}
        title={this.props.title}
      />
    );

    return (
      <DocumentTitle title={this._title()}>
        <UIRouter router={this._router}>
          <UIView render={app as StatelessComponent<any>} />
        </UIRouter>
      </DocumentTitle>
    );
  }

  private _title = () => (
    this.props.tabs[this.props.tabIndex] ?
      this.props.tabs[this.props.tabIndex].id :
      this.props.title
  )
}
