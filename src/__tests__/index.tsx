import {UIRouterReact} from '@uirouter/react';
import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import UIRouterReactDigest from '../';

const drawerHeader = () => (<div>Drawer Header</div>);
const footer = () => (<div>Footer</div>);
const header = () => (<div>Header</div>);

test('Initializing component', () => {
  const component = shallow(
    <UIRouterReactDigest
      debug={defaults.debug}
      drawerDocked={defaults.drawerDocked}
      drawerDrag={defaults.drawerDrag}
      drawerHeader={drawerHeader}
      drawerHover={defaults.drawerHover}
      drawerIndex={defaults.drawerIndex}
      drawerOpen={defaults.drawerOpen}
      drawers={testDefaults.drawers}
      footer={footer}
      header={header}
      onDrawerSelect={defaults.onDrawerSelect}
      onDrawerOpenToggle={defaults.onDrawerOpenToggle}
      onTabSelect={defaults.onTabSelect}
      orientation={defaults.orientation}
      plugins={defaults.plugins}
      rootState={defaults.rootState}
      router={new UIRouterReact()}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
      title={defaults.title}
    />
  );
  expect(component).toMatchSnapshot();
});
