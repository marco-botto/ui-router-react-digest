import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import Drawer from '../';

test('Drawer init', () => {
  const component = shallow(
    <Drawer
      drawerIndex={defaults.currentDrawer}
      drawers={testDefaults.drawers}
      onSelect={defaults.onDrawerSelect}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
    />
  );
  expect(component).toMatchSnapshot();
});
