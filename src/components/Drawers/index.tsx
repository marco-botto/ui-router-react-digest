import {UIView} from '@uirouter/react';
import {
  Drawer as IDrawer,
  Drawers as IDrawers,
  Orientation,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {ComponentClass, PureComponent, StatelessComponent} from 'react';
import SwipeableViews from 'react-swipeable-views';
import {IDrawerHeaderProps} from 'src/components/_defaults/DrawerHeader';
import DrawerContext from './DrawerContext';
import './Drawers.css';

export interface IDrawersProps {
  drawerIndex: number;
  drawers: IDrawers;
  onSelect: (drawerIndex: number) => void;
  orientation: Orientation;
  tabIndex: number;
  tabs: ITabs;
}

export default class Drawers extends PureComponent<IDrawersProps> {
  public static propTypes = {
    drawerIndex: PropTypes.number.isRequired,
    drawers: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired).isRequired,
    onSelect: PropTypes.func.isRequired,
    orientation: PropTypes.string.isRequired,
    tabIndex: PropTypes.number.isRequired,
    tabs: PropTypes.array
  };

  public render() {
    const drawerHeader = (DrawerHeader: ComponentClass<IDrawerHeaderProps>) => (
      <DrawerHeader
        currentDrawer={this.props.drawerIndex}
        drawers={this.props.drawers}
        onDrawerSelect={this.props.onSelect}
      />
    );

    const contexts = this.props.drawers.map((drawer: IDrawer, index: number) => (
      <DrawerContext
        currentDrawer={index}
        drawer={drawer}
        drawerIndex={this.props.drawerIndex}
        key={drawer.id}
        orientation={this.props.orientation}
        tabIndex={this.props.tabIndex}
        tabs={this.props.tabs}
      />
    ));

    return (
      <div styleName='drawers'>
        <UIView
          name='drawerHeader'
          render={drawerHeader as StatelessComponent<any>}
        />
        <SwipeableViews
          index={this.props.drawers[this.props.drawerIndex] ? this.props.drawerIndex : 0}
          onChangeIndex={this.props.onSelect}
          styleName='drawer-swipe'
        >
          {contexts}
        </SwipeableViews>
      </div>
    );
  }
}
