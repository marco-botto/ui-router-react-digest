import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import DrawerContext from '../';

test('Drawer content tabs', () => {
  const component = shallow(
    <DrawerContext
      currentDrawer={defaults.currentDrawer}
      drawer={testDefaults.drawers[0]}
      drawerIndex={0}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
    />
  );
  expect(component).toMatchSnapshot();
});

// Need to figure out how to use touch events on shallow
/*
interface SwipeComponent extends Component<any, any> {
  viewLength?: number;
  startIndex?: number;
}

test('Select 2nd tab', () => {
  const tabIndex = 1;
  const whenDrawerSelect = jest.fn();

  const content = shallow(
    <DrawerContent
      currentDrawer={defaults.currentDrawer}
      currentTab={defaults.currentTab}
      drawers={testDefaults.drawers}
      onSwipe={whenDrawerSelect}
      orientation={defaults.orientation}
      tabs={testDefaults.tabs}
    />
  );

  content.simulate(
    'touchStart',
    {touches: [{
      pageX: 50,
      pageY: 0,
    }]}
  );
  content.simulate(
    'touchEnd',
    {touches: [{
      pageX: 100,
      pageY: 0,
    }]}
  );

  const instance: SwipeComponent = content.instance();
  instance.viewLength = 200;
  instance.startIndex = 0;

  expect(whenDrawerSelect).toBeCalledWith(tabIndex);
});
*/
