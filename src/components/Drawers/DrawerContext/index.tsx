import {UIView} from '@uirouter/react';
import {
  Drawer as IDrawer,
  Orientation,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import SwipeableViews from 'react-swipeable-views';
import Content from 'src/components/Content';
import './DrawerContext.css';

export interface IDrawerContextProps {
  currentDrawer: number;
  drawer: IDrawer;
  drawerIndex: number;
  orientation: Orientation;
  tabIndex: number;
  tabs: ITabs;
}

export default class DrawerContext extends PureComponent<IDrawerContextProps> {
  public static propTypes = {
    drawer: PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired,
    drawerIndex: PropTypes.number.isRequired,
    orientation: PropTypes.string.isRequired,
    tabIndex: PropTypes.number.isRequired,
    tabs: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired).isRequired
  };

  public render () {
    const isCurrentDrawer = this.props.currentDrawer === this.props.drawerIndex;

    const drawerTabs = this.props.tabs.map((tab, index) => {
      const isCurrentTab = index === this.props.tabIndex;

      return (
        <div
          key={tab.id}
          styleName='drawer'
        >
          <Content
            hidden={!(isCurrentDrawer && isCurrentTab)}
            name={tab.id + '_' + this.props.drawer.id}
            orientation={this.props.orientation}
          />
        </div>
      );
    });

    return (
      this.props.drawer.contextual ? (
        <SwipeableViews
          disabled={true}
          index={this.props.tabs[this.props.tabIndex] ? this.props.tabIndex : 0}
          styleName='drawer-swipe'
        >
          {drawerTabs}
        </SwipeableViews>
      ) : (
        <Content
          hidden={!isCurrentDrawer}
          name={this.props.drawer.id}
          orientation={this.props.orientation}
        />
      )
    );
  }
}
