import {UIView} from '@uirouter/react';
import {
  Orientation,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import SwipeableViews from 'react-swipeable-views';
import Content from 'src/components/Content';
import defaults from 'src/defaults';
import './Tabs.css';

export interface ITabsProps {
  onTabSelect: (tabIndex: number) => void;
  orientation: Orientation;
  tabIndex: number;
  tabs: ITabs;
}

export default class Tabs extends PureComponent<ITabsProps> {
  public static propTypes = {
    onTabSelect: PropTypes.func,
    orientation: PropTypes.string,
    tabIndex: PropTypes.number,
    tabs: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired).isRequired
  };

  public static defaultProps = {
    onTabSelect: defaults.onTabSelect,
    orientation: defaults.orientation,
    tabIndex: defaults.tabIndex,
    tabs: defaults.tabs
  };

  public render() {
    const views = this.props.tabs.map((tab, index) => {
      return (
        <div
          key={tab.id}
          styleName='tab'
        >
          <Content
            hidden={index !== this.props.tabIndex}
            name={tab.id}
            // Reverse orientation to whatever sidebar is
            orientation={this.props.orientation === 'left' ? 'right' : 'left'}
          />
        </div>
      );
    });

    const disabled = this.props.tabs[this.props.tabIndex] &&
      !this.props.tabs[this.props.tabIndex].params.swipe;

    return (
      <SwipeableViews
        disabled={disabled}
        index={this.props.tabIndex}
        onChangeIndex={this.props.onTabSelect}
        styleName='tab-swipe'
      >
        {views}
      </SwipeableViews>
    );
  }
}
