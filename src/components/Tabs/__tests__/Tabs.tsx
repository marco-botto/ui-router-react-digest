import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import Tabs from '../';

test('Main content tabs', () => {
  const component = shallow(
    <Tabs
      onTabSelect={defaults.onTabSelect}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
    />
  );
  expect(component).toMatchSnapshot();
});
