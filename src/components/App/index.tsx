import {UIView} from '@uirouter/react';
import {
  Drawers as IDrawers,
  Orientation,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {
  ComponentClass,
  CSSProperties,
  KeyboardEvent,
  PureComponent,
  StatelessComponent,
  SyntheticEvent
} from 'react';
import Sidebar from 'react-sidebar';
import {IHeaderProps} from 'src/components/_defaults/Header';
import Drawers from 'src/components/Drawers';
import {ITabsProps} from 'src/components/Tabs';
import defaults from 'src/defaults';
import './App.css';

export interface IAppProps {
  drawerDocked: boolean;
  drawerDrag: boolean;
  drawerHover: boolean;
  drawerIndex: number;
  drawerOpen: boolean;
  drawers: IDrawers;
  onDrawerOpenToggle: (drawerIndex?: boolean) => void;
  onDrawerSelect: (drawerIndex: number) => void;
  onTabSelect: (tabIndex: number) => void;
  orientation: Orientation;
  tabIndex: number;
  tabs: ITabs;
  title: string;
}

/* Tried using classnames instead but ran into bugs */
const sidebarStyles: {[key: string]: CSSProperties} = {
  content: {
    minWidth: '340px',
    overflow: 'auto',
    transition: 'left 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, ' +
      'right 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0)',
    transition: 'opacity 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, ' +
      'visibility 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
  },
  root: {
    height: '100%',
    position: 'relative'
  },
  sidebar: {
    transition: 'transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    width: '300px'
  }
};

export default class App extends PureComponent<IAppProps> {
  public static propTypes = {
    drawerDocked: PropTypes.bool,
    drawerDrag: PropTypes.bool,
    drawerHover: PropTypes.bool,
    drawerIndex: PropTypes.number,
    drawerOpen: PropTypes.bool,
    drawers: PropTypes.array,
    onDrawerOpenToggle: PropTypes.func.isRequired,
    onDrawerSelect: PropTypes.func.isRequired,
    onTabSelect: PropTypes.func.isRequired,
    orientation: PropTypes.string,
    tabIndex: PropTypes.number,
    tabs: PropTypes.array,
    title: PropTypes.string
  };

  public static defaultProps = {
    drawerDocked: defaults.drawerDocked,
    drawerDrag: defaults.drawerDrag,
    drawerHover: defaults.drawerHover,
    drawerIndex: defaults.drawerIndex,
    drawerOpen: defaults.drawerOpen,
    drawers: defaults.drawers,
    onDrawerOpenToggle: defaults.onDrawerOpenToggle,
    onDrawerSelect: defaults.onDrawerSelect,
    onTabSelect: defaults.onTabSelect,
    orientation: defaults.orientation,
    tabIndex: defaults.tabIndex,
    tabs: defaults.tabs,
    title: defaults.title
  };

  public render() {
    const header = (Header: ComponentClass<IHeaderProps>) => (
      <Header
        drawerDocked={this.props.drawerDocked}
        drawerOpen={this.props.drawerOpen}
        onDrawerOpenToggle={this.props.onDrawerOpenToggle}
        onTabSelect={this.props.onTabSelect}
        orientation={this.props.orientation}
        tabIndex={this.props.tabIndex}
        tabs={this.props.tabs}
        title={this.props.title}
      />
    );

    const sidebar = (
      <Drawers
        drawerIndex={this.props.drawerIndex}
        drawers={this.props.drawers}
        onSelect={this.props.onDrawerSelect}
        orientation={this.props.orientation}
        tabIndex={this.props.tabIndex}
        tabs={this.props.tabs}
      />
    );

    const content = (Content: ComponentClass<ITabsProps>) => (
      <Content
        onTabSelect={this.props.onTabSelect}
        orientation={this.props.orientation}
        tabIndex={this.props.tabIndex}
        tabs={this.props.tabs}
      />
    );

    return (
      <div styleName='app'>
        <UIView
          name='header'
          render={header as StatelessComponent<any>}
        />
        <Sidebar
          docked={this.props.drawerHover && this.props.drawerOpen}
          onSetOpen={this.onSetOpen}
          open={this.props.drawerOpen}
          pullRight={this.props.orientation === 'right'}
          sidebar={sidebar}
          styles={sidebarStyles}
          touch={this.props.drawerDrag}
        >
          <div
            aria-hidden={!this.props.drawerDocked || !this.props.drawerOpen}
            onClick={this.onContentSelect}
            onKeyPress={this.onContentPress}
            styleName='main'
            tabIndex={!this.props.drawerDocked && this.props.drawerOpen ? 0 : -1}
          >
            <UIView
              name='tabs'
              render={content as StatelessComponent<any>}
            />
          </div>
        </Sidebar>
      </div>
    );
  }

  private onContentSelect = (event: SyntheticEvent<Element>) => {
    event.preventDefault();
    if (!this.props.drawerDocked && this.props.drawerOpen) {
      this.props.onDrawerOpenToggle();
    }
  }

  private onContentPress = (event: KeyboardEvent<Element>) => {
    event.preventDefault();
    if (
      !this.props.drawerDocked &&
      this.props.drawerOpen &&
      (event.keyCode === 0 || event.keyCode === 32)
    ) {
      this.props.onDrawerOpenToggle();
    }
  }

  private onSetOpen = (): {} => {
    this.props.onDrawerOpenToggle();
    return {};
  }
}
