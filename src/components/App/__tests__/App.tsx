import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import App from '../';

test('The initial app view component', () => {
  const component = shallow(
    <App
      drawerDocked={defaults.drawerDocked}
      drawerDrag={defaults.drawerDrag}
      drawerHover={defaults.drawerHover}
      drawerIndex={defaults.drawerIndex}
      drawerOpen={defaults.drawerOpen}
      drawers={testDefaults.drawers}
      onDrawerSelect={defaults.onDrawerSelect}
      onDrawerOpenToggle={defaults.onDrawerOpenToggle}
      onTabSelect={defaults.onTabSelect}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
      title={defaults.title}
    />
  );
  expect(component).toMatchSnapshot();
});
