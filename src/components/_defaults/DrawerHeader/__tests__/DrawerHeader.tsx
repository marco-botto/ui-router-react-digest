import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/testDefaults';
import DrawerHeader from '../';

test('Drawer populates tabs with given array', () => {
  const currentDrawer = 1;
  const onDrawerSelect = () => {return;};

  const header = shallow(
    <DrawerHeader
      currentDrawer={currentDrawer}
      drawers={defaults.drawers}
      onDrawerSelect={onDrawerSelect}
    />
  );
  expect(header).toMatchSnapshot();
  expect(
    header.find('[name="' + defaults.drawers[currentDrawer].params.title + '"]'
  ).prop('disabled')).toEqual(true);
  expect(header.find(
    '[name="' +
      defaults.drawers[
        currentDrawer === defaults.drawers.length - 1 ?
          0 :
          defaults.drawers.length - 1
      ].params.title +
      '"]'
  ).prop('disabled')).toEqual(false);
});

test('Select 2nd tab', () => {
  const tabIndex = 1;
  const whenDrawerSelect = jest.fn();

  const header = shallow(
    <DrawerHeader
      currentDrawer={0}
      drawers={defaults.drawers}
      onDrawerSelect={whenDrawerSelect}
    />
  );

  header.find('[name="' + defaults.drawers[tabIndex].params.title + '"]').simulate('click');

  expect(whenDrawerSelect).toBeCalledWith(tabIndex);
});

test('Drawer populates tabs with array of length 1', () => {
  const onDrawerSelect = () => {return;};

  const header = shallow(
    <DrawerHeader
      currentDrawer={0}
      drawers={[defaults.drawers[0]]}
      onDrawerSelect={onDrawerSelect}
    />
  );
  expect(header).toMatchSnapshot();
});

test('Drawer populates tabs without titles', () => {
  const drawers = defaults.drawers.map(drawer => ({
    ...drawer,
    params: {
      ...drawer.params,
      title: undefined
    }
  }));
  const onDrawerSelect = () => {return;};

  const header = shallow(
    <DrawerHeader
      currentDrawer={0}
      drawers={drawers}
      onDrawerSelect={onDrawerSelect}
    />
  );
  expect(header).toMatchSnapshot();
});
