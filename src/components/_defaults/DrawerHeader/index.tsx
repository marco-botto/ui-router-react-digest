import {
  Drawers as IDrawers,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent, WheelEvent} from 'react';
import ReactDOM from 'react-dom';
import defaults from 'src/defaults';
import './DrawerHeader.css';

export interface IDrawerHeaderProps {
  currentDrawer: number;
  drawers: IDrawers;
  onDrawerSelect: (index: number) => void;
}

const HORIZONTAL_SCROLL_MINIMUM = 53;

class DrawerHeader extends PureComponent<IDrawerHeaderProps> {

  public static propTypes = {
    currentDrawer: PropTypes.number.isRequired,
    drawers: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      params: PropTypes.shape({
        title: PropTypes.string
      })
    }).isRequired).isRequired,
    onDrawerSelect: PropTypes.func.isRequired
  };

  public static defaultProps = {
    currentDrawer: defaults.drawerIndex,
    drawers: defaults.drawers,
    onDrawerSelect: defaults.onDrawerSelect
  };

  private node: Element;

  public render() {

    const drawerList = this.props.drawers.length > 1 ?
      this.props.drawers.map((drawer, index) => {
        return (
          <button
            disabled={index === this.props.currentDrawer}
            key={drawer.id}
            onClick={this.onClick(index)}
            name={drawer.params.title ? drawer.params.title : drawer.id}
          >
            {drawer.params.title ? drawer.params.title : drawer.id}
          </button>
        );
      }) : null;

    return (
      <div
        onWheel={this.handleScroll}
        ref={this.setRef}
        styleName='drawer-header'
      >
        {drawerList}
      </div>
    );
  }

  private setRef = (ref: Element) => {
    this.node = ref;
  }

  private handleScroll = (event: WheelEvent<HTMLDivElement>) => {
    event.preventDefault();
    this.node.scrollLeft = this.node.scrollLeft +
      (// Prevent scroll interval less than 53px
        Math.abs(event.deltaY) > HORIZONTAL_SCROLL_MINIMUM ?
          event.deltaY :
          HORIZONTAL_SCROLL_MINIMUM * (event.deltaY < 0 ? -1 : 1)
      );
  }

  private onClick = (index: number) => () => {
    return this.props.onDrawerSelect(index);
  }
}

export default DrawerHeader;
