import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import './DrawerToggle.css';

export interface IDrawerToggleProps {
  onToggle: () => void;
}

export default class DrawerToggle extends PureComponent<IDrawerToggleProps> {
  public static propTypes = {
    onToggle: PropTypes.func.isRequired
  };

  public render() {
    return (
      <button
        onClick={this.onClick}
        styleName='drawer-toggle-button'
      >
        Toggle Drawer
      </button>
    );
  }

  private onClick = () => (this.props.onToggle());
}
