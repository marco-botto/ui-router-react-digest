import {shallow} from 'enzyme';
import React from 'react';
import renderer from 'react-test-renderer';
import DrawerToggle from '../';

test('DrawerToggle toggles the open/close state of the drawer', () => {
  const onToggle = () => {return;};

  const component = renderer.create(
    <DrawerToggle
      onToggle={onToggle}
    />
  );
  const drawerToggle = component.toJSON();
  expect(drawerToggle).toMatchSnapshot();
});

test('Toggle drawer', () => {
  const onToggle = jest.fn();

  const drawerToggle = shallow(
    <DrawerToggle
      onToggle={onToggle}
    />
  );

  drawerToggle.simulate('click');

  expect(onToggle).toBeCalled();
});
