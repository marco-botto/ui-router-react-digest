import {Orientation} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import './Title.css';

export interface ITitleProps {
  orientation?: Orientation;
  title?: string;
}

export default class Title extends PureComponent<ITitleProps> {
  public static propTypes = {
    orientation: PropTypes.string,
    title: PropTypes.string
  };

  public static defaultProps = {
    orientation: 'left',
    title: 'Default Title'
  };

  public render() {
    return (
      <h3 styleName={'header-title-' + this.props.orientation}>
        {this.props.title}
      </h3>
    );
  }
}
