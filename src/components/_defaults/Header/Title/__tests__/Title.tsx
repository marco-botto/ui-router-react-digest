import React from 'react';
import renderer from 'react-test-renderer';
import Title from '../';

const text = 'Test Title';

test('Title given title', () => {
  const component = renderer.create(
    <Title
      title={text}
    />
  );
  const title = component.toJSON();
  expect(title).toMatchSnapshot();
});

test('Title given empty', () => {
  const component = renderer.create(
    <Title />
  );
  const title = component.toJSON();
  expect(title).toMatchSnapshot();
});
