import {
  Orientation,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import defaults from 'src/defaults';
import DrawerToggle from './DrawerToggle';
import './Header.css';
import TabBar from './TabBar';
import Title from './Title';

export interface IHeaderProps {
  drawerDocked: boolean;
  drawerOpen: boolean;
  onDrawerOpenToggle: () => void;
  onTabSelect: (index: number) => void;
  orientation: Orientation;
  tabs: ITabs;
  tabIndex: number;
  title: string;
}

export default class Header extends PureComponent<IHeaderProps> {
  public static propTypes = {
    drawerDocked: PropTypes.bool,
    drawerOpen: PropTypes.bool,
    onDrawerOpenToggle: PropTypes.func,
    onTabSelect: PropTypes.func,
    orientation: PropTypes.string,
    tabIndex: PropTypes.number,
    tabs: PropTypes.array,
    title: PropTypes.string
  };

  public static defaultProps = {
    drawerDocked: defaults.drawerDocked,
    drawerOpen: defaults.drawerOpen,
    onDrawerOpenToggle: defaults.onDrawerOpenToggle,
    onTabSelect: defaults.onTabSelect,
    orientation: defaults.orientation,
    tabIndex: defaults.tabIndex,
    tabs: defaults.tabs,
    title: defaults.title
  };

  public render() {
    return (
      <div styleName={'header-' + this.props.orientation}>
        <DrawerToggle
          onToggle={this.props.onDrawerOpenToggle}
        />
        <div styleName={'header-subheader-' + this.props.orientation}>
          <Title
            orientation={this.props.orientation}
            title={this.props.title}
          />
          <TabBar
            currentTab={this.props.tabIndex}
            onTabSelect={this.props.onTabSelect}
            orientation={this.props.orientation}
            tabs={this.props.tabs}
          />
        </div>
      </div>
    );
  }
}
