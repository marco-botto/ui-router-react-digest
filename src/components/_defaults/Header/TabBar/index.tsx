import {
  Orientation,
  Tab as ITab,
  Tabs as ITabs,
} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent, WheelEvent} from 'react';
import './TabBar.css';

export interface ITabBarProps {
  currentTab: number;
  onTabSelect: (index: number) => void;
  orientation?: Orientation;
  tabs: ITabs;
}

const HORIZONTAL_SCROLL_MINIMUM = 53;

export default class TabBar extends PureComponent<ITabBarProps> {
  public static propTypes = {
    currentTab: PropTypes.number.isRequired,
    onTabSelect: PropTypes.func.isRequired,
    orientation: PropTypes.string,
    tabs: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      params: PropTypes.shape({
        title: PropTypes.string
      })
    }).isRequired).isRequired
  };

  public static defaultProps = {
    orientation: 'left'
  };

  private node: Element;

  public render() {
    let filterCount: number = 0;
    const tabList = this.props.tabs.filter((tab) => {
      if (tab.params.hidden) {
        filterCount += 1;
        return false;
      }
      return true;
    }).map((tab, index) => {
      const title = this.tabName(tab);
      return (
        <button
          disabled={index + filterCount === this.props.currentTab}
          key={tab.id}
          name={title}
          onClick={this.onClick(index + filterCount)}
        >
          {title}
        </button>
      );
    });

    return (
      <span
        onWheel={this.handleScroll}
        ref={this.setRef}
        styleName={'header-tabs-' + this.props.orientation}
      >
        {tabList}
      </span>
    );
  }

  private tabName = (tab: ITab) => (
    tab.params && tab.params.title ?
      tab.params.title :
      tab.id
  )

  private onClick = (index: number) => () => (this.props.onTabSelect(index));

  private setRef = (ref: Element) => {
    this.node = ref;
  }

  private handleScroll = (event: WheelEvent<HTMLDivElement>) => {
    event.preventDefault();
    this.node.scrollLeft = this.node.scrollLeft +
      (// Prevent scroll interval less than 53px
        Math.abs(event.deltaY) > HORIZONTAL_SCROLL_MINIMUM ?
          event.deltaY :
          HORIZONTAL_SCROLL_MINIMUM * (event.deltaY < 0 ? -1 : 1)
      );
  }
}
