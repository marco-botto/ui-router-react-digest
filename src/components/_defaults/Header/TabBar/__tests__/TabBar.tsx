import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/testDefaults';
import TabBar from '../';

test('Drawer populates tabs with given array', () => {
  const currentTab = 1;

  const onTabSelect = () => {return;};

  const tabs = shallow(
    <TabBar
      currentTab={currentTab}
      onTabSelect={onTabSelect}
      tabs={defaults.tabs}
    />
  );
  expect(tabs).toMatchSnapshot();

  expect(tabs.find(
    '[name="' + defaults.tabs[currentTab].params.title + '"]'
  ).prop('disabled')).toEqual(true);
  expect(tabs.find(
    '[name="' +
      defaults.tabs[
        currentTab === defaults.tabs.length - 1 ?
          0 :
          defaults.tabs.length - 1
      ].params.title +
      '"]'
  ).prop('disabled')).toEqual(false);
});

test('Select 2nd tab', () => {
  const tabIndex = 1;
  const whenTabSelect = jest.fn();

  const tabs = shallow(
    <TabBar
      currentTab={tabIndex}
      onTabSelect={whenTabSelect}
      tabs={defaults.tabs}
    />
  );

  tabs.find('[name="' + defaults.tabs[tabIndex].params.title + '"]').simulate('click');

  expect(whenTabSelect).toBeCalledWith(tabIndex);
});

test('Drawer populates tabs without titles', () => {

  const onTabSelect = () => {return;};
  const tabs = defaults.tabs.map(tab => ({
    ...tab,
    params: {
      ...tab.params,
      title: undefined
    }
  }));

  const header = shallow(
    <TabBar
      currentTab={0}
      onTabSelect={onTabSelect}
      tabs={tabs}
    />
  );
  expect(header).toMatchSnapshot();
});
