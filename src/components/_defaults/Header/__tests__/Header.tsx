import {shallow} from 'enzyme';
import React from 'react';
import Header from '../';
import defaults from '../../../../defaults';
import testDefaults from '../../../../testDefaults';

test('Default header with DrawerToggle, TabBar, and Title', () => {
  const component = shallow(
    <Header
      drawerDocked={defaults.drawerDocked}
      drawerOpen={defaults.drawerOpen}
      onDrawerOpenToggle={defaults.onDrawerOpenToggle}
      onTabSelect={defaults.onTabSelect}
      orientation={defaults.orientation}
      tabIndex={defaults.tabIndex}
      tabs={testDefaults.tabs}
      title={defaults.title}
    />
  );
  expect(component).toMatchSnapshot();
});
