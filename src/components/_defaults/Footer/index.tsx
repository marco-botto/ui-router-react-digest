import React, {PureComponent} from 'react';
import './Footer.css';

export default class Footer extends PureComponent {
  public render() {
    return(
      <div styleName='footer'>
        <h4>Footer</h4>
      </div>
    );
  }
}
