import React from 'react';
import renderer from 'react-test-renderer';
import Footer from '../';

test('Generic footer for testing', () => {
  const component = renderer.create(
    <Footer />
  );
  const footer = component.toJSON();
  expect(footer).toMatchSnapshot();
});
