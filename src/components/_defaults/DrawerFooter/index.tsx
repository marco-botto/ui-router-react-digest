import React, {PureComponent} from 'react';
import './DrawerFooter.css';

export default class DrawerFooter extends PureComponent {
  public render() {
    return(
      <div styleName='drawer-footer'>
        <h4>Drawer Footer</h4>
      </div>
    );
  }
}
