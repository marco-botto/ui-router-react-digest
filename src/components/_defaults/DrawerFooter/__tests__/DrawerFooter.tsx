import React from 'react';
import renderer from 'react-test-renderer';
import DrawerFooter from '../';

test('Generic drawer footer for testing', () => {
  const component = renderer.create(
    <DrawerFooter />
  );
  const footer = component.toJSON();
  expect(footer).toMatchSnapshot();
});
