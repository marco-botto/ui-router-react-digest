import {shallow} from 'enzyme';
import React from 'react';
import defaults from 'src/defaults';
import testDefaults from 'src/testDefaults';
import Content from '../';

test('Generic content container', () => {
  const component = shallow(
    <Content
      hidden={false}
      orientation={defaults.orientation}
      name={'any'}
    />
  );
  expect(component).toMatchSnapshot();
});
