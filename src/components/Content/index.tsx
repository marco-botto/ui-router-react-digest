import {UIView} from '@uirouter/react';
import {Orientation} from 'index';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import SwipeableViews from 'react-swipeable-views';
import './Content.css';

interface IContentProps {
  hidden: boolean;
  name: string;
  orientation: Orientation;
}

interface IContentState {
  hidden: boolean;
}

export default class Content extends PureComponent<IContentProps, IContentState> {
  public static propTypes = {
    hidden: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    orientation: PropTypes.string.isRequired
  };

  constructor (props: IContentProps) {
    super(props);

    this.state = {hidden: props.hidden};
  }

  public componentDidUpdate () {
    if (this.props.hidden) {
      setTimeout(() => {this.setState({hidden: this.props.hidden});}, 300);
    } else {
      this.setState({hidden: this.props.hidden});
    }
  }

  public render () {
    return (
      <div styleName={'content-direction-' + this.props.orientation}>
        <div
          aria-hidden={this.state.hidden}
          styleName='content-container'
          hidden={this.state.hidden}
        >
          <div styleName='content'>
            <UIView name={this.props.name}/>
          </div>
          <UIView name='footer' />
        </div>
        <div styleName={'content-mask-' + this.props.orientation} />
      </div>
    );
  }
}
