import {UIRouterPluginBase} from '@uirouter/react';
import {Orientation} from 'index';

export default {
  currentDrawer: 0,
  debug: false,
  drawerDocked: true,
  drawerDrag: false,
  drawerHover: false,
  drawerIndex: 0,
  drawerOpen: false,
  drawers: [],
  onDrawerOpenToggle: () => {return;},
  onDrawerSelect: () => {return;},
  onTabSelect: () => {return;},
  orientation: 'left' as Orientation,
  plugins: [] as UIRouterPluginBase[],
  rootState: {name: 'uurd'},
  tabIndex: 0,
  tabs: [],
  title: 'Digest'
};
