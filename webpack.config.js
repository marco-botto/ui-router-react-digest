/*
  eslint-disable
  import/unambiguous,
  import/no-commonjs,
  filenames/match-exported,
  filenames/match-regex
*/
const webpackDigest = require('webpack-digest/lib/webpack.config.js');

module.exports = webpackDigest;
/*
  eslint-enable
  import/unambiguous,
  import/no-commonjs,
  filenames/match-exported,
  filenames/match-regex
*/
